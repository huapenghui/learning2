#!/usr/bin/env python 
# -*- coding:utf-8 -*-
"""
url 去重 要考虑 到  url 比如 get 请求中，是否包含 时间戳，若包含时间戳，则无法做到url去重，


安装
pip install bitarray-0.8.3-cp35-cp35m-win_amd64.whl
pip install pybloom_live

该模块包含两个类实现布隆过滤器功能。
BloomFilter 是定容。
ScalableBloomFilter 可以自动扩容

"""


'''
使用ScalableBloomFilter
from pybloom_live import ScalableBloomFilter
sbf=ScalableBloomFilter(initial_capacity=100, error_rate=0.001, 
mode=ScalableBloomFilter.LARGE_SET_GROWTH)
url = "www.baidu.com"
url2 = "www.douban,com"
sbf.add(url)

print(url in sbf) # True
print(url2 in sbf) # False
BloomFilter
from pybloom_live import BloomFilter
bf = BloomFilter(capacity=1000)
bf.add("www.baidu.com")
print("www.baidu.com" in bf) # True
print("www.douban.com" in bf) # False


'''



"""

去重操作
from pybloom_live import ScalableBloomFilter
定义集合
self.sbf = ScalableBloomFilter(initial_capacity=100, error_rate=0.001, 
mode=ScalableBloomFilter.LARGE_SET_GROWTH)
去重操作
#判断照片页面的 url 是否已经存在于集合中，不在就运行 else 下的内容，在则忽略。
if url in self. sbf: 
 print(u'这个页面已经爬取过了')
else:
#处理操作
添加数据：
self. sbf.add(url)
"""



# ScalableBloomFilter
# 实现自动扩容
# from pybloom_live import ScalableBloomFilter
# sbf=ScalableBloomFilter(initial_capacity=100, error_rate=0.001,
# mode=ScalableBloomFilter.LARGE_SET_GROWTH)
# url = "www.baidu.com"
# url2 = "www.douban,com"
# sbf.add(url)
#
# print(url in sbf) # True
# print(url2 in sbf) # False

# BloomFilter
# 定容
from pybloom_live import BloomFilter
bf = BloomFilter(capacity=1000)
bf.add("www.baidu.com")
print("www.baidu.com" in bf) # True
print("www.douban.com" in bf) # False

from pybloom_live import ScalableBloomFilter
sbf = ScalableBloomFilter(initial_capacity=100, error_rate=0.001,
mode=ScalableBloomFilter.LARGE_SET_GROWTH)

if url in sbf:
    print(u'这个页面已经爬取过了')
else:
    print('新的url')
#处理操作