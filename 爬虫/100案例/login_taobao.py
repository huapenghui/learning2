#!/usr/bin/env python 
# -*- coding:utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
import time

# 淘宝经过短信验证码 以后，不会出现滑块儿，直接验证通过  可以成功登陆
# driver_path = 'D:\\Program Files (x86)\\chromedriver.exe'

class TaobaoLogin:
    def __init__(self):
        self.url_login = 'https://login.taobao.com/member/login.jhtml'
        self.url_home = 'https://www.taobao.com'
        options = webdriver.ChromeOptions()
        options.add_experimental_option('excludeSwitches', ['enable-automation'])
        # self.browser = webdriver.Chrome(options=options, executable_path=driver_path)
        self.browser = webdriver.Chrome(options=options)
        with open('stealth.min.js') as f:
            js = f.read()
        self.browser.execute_cdp_cmd("Page.addScriptToEvaluateOnNewDocument", {
            "source": js
        })
        self.browser.maximize_window()
        self.wait = WebDriverWait(self.browser, 10)

    def login(self, username, password):
        # 登录并转到首页
        self.browser.get(self.url_login)
        self.wait.until(EC.presence_of_element_located((By.ID, 'fm-login-id'))).send_keys(username)
        self.wait.until(EC.presence_of_element_located((By.ID, 'fm-login-password'))).send_keys(password)
        self.verify()
        time.sleep(3)
        home_page = self.wait.until(EC.presence_of_element_located((By.CSS_SELECTOR, '.site-nav-bd > ul.site-nav-bd-r > li.site-nav-home > div > a > span')))
        home_page.click()
        time.sleep(3)

    def verify(self):
        #向右拖动滑块验证
        button = self.browser.find_element_by_class_name('fm-btn')
        #  点击登陆
        button.click()
        time.sleep(2)
        if self.browser.find_element_by_class_name('s-name'):
            print('success')
            cookie = self.browser.get_cookies()
            print(cookie)
        else:
            #  查找滑块
            slider = self.wait.until(EC.presence_of_element_located((By.ID, 'nc_1_n1z')))
            time.sleep(5)
            ActionChains(self.browser).click_and_hold(on_element=slider).perform()
            ActionChains(self.browser).move_by_offset(xoffset=280, yoffset=0).perform()
            ActionChains(self.browser).release().perform()
            time.sleep(1)
            button.click()
            time.sleep(3)
            if self.browser.current_url == self.url_login:
                self.verify()

if __name__ == '__main__':
    t = TaobaoLogin()
    t.login('18790579029', 'hph@6927962318')


