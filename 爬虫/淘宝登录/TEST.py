#!/usr/bin/env python 
# -*- coding:utf-8 -*-
from selenium import webdriver
from time import sleep
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import WebDriverException
from selenium.webdriver import ActionChains
import pyautogui
import pymsgbox
pyautogui.PAUSE = 0.5


# 定义一个淘宝类
class TaoBao(object):
    def __init__(self):
        options = webdriver.ChromeOptions()
        # options.add_argument('--headless')
        options.add_experimental_option("excludeSwitches", ["enable-automation"])
        options.add_experimental_option('useAutomationExtension', False)
        self.browser = webdriver.Chrome(options=options)
        self.domain = 'http://www.taobao.com'
        # self.browser.maximize_window()  # 窗口最大化
        self.browser.implicitly_wait(5)
        self.browser.execute_cdp_cmd("Page.addScriptToEvaluateOnNewDocument", {
            "source": """
                    Object.defineProperty(navigator, 'webdriver', {
                      get: () => undefined
                    })
                  """
        })

    def open(self):
        self.browser.get(self.domain)
        sleep(1)

    def login(self, usrname, passwd):

        self.browser.find_element_by_xpath('//*[@id="J_SiteNavLogin"]/div[1]/div[1]/a[1]').click()  # 点击登录按钮
        self.browser.find_element_by_xpath('//*[@id="fm-login-id"]').send_keys(usrname)  # 输入账号
        self.browser.find_element_by_xpath('//*[@id="fm-login-password"]').send_keys(passwd)  # 输入密码

        # 若出现滑块，需要模拟滑动
        sleep(1)
        try:
            # 出现验证码，滑动验证
            slider = self.browser.find_element_by_xpath("//span[contains(@class, 'btn_slide')]")
            if slider.is_displayed():
                # 拖拽滑块
                ActionChains(self.browser).drag_and_drop_by_offset(slider, 258, 0).perform()
                sleep(0.5)
                # 释放滑块，相当于点击拖拽之后的释放鼠标
                ActionChains(self.browser).release().perform()
        except (NoSuchElementException, WebDriverException):
            pass

        # 模拟人工点击
        try:
            coords = pyautogui.locateOnScreen('1.png')
            x, y = pyautogui.center(coords)
            pyautogui.leftClick(x, y)
        except TypeError:
            pymsgbox.alert('未找到登录按钮')

        # 验证登录结果
        nickname = self.get_nickname()
        if nickname:
            pymsgbox.alert('登录成功，呢称为:' + nickname)

    def get_nickname(self):
        self.browser.get(self.domain)
        sleep(0.5)
        try:
            return self.browser.find_element_by_class_name('site-nav-user').text
        except NoSuchElementException:
            return ''


# main函数入口
if __name__ == "__main__":
    tb = TaoBao()
    tb.open()
    usrname = 'qianyipei'
    passwd = 'qian120920hu'
    tb.login(usrname, passwd)
# import asyncio
# from pyppeteer import launch
#
#
# async def main():
#     browser = await launch()
#     page = await browser.newPage()
#     # await page.goto('https://zhangslob.github.io/')
#     await page.goto( 'http://www.taobao.com')
#     await page.screenshot({'path': 'zhangslob.png'})
#     await browser.close()
# asyncio.get_event_loop().run_until_complete(main())


import asyncio
import time, random
from pyppeteer.launcher import launch  # 控制模拟浏览器用
from retrying import retry  # 设置重试次数用的


async def main(username, pwd, url):  # 定义main协程函数，
    # 以下使用await 可以针对耗时的操作进行挂起
    browser = await launch({'headless': False, 'args': ['--no-sandbox'],'userDataDir':'E:\WeSingCache\cache' })  # 启动pyppeteer 属于内存中实现交互的模拟器
    page = await browser.newPage()  # 启动个新的浏览器页面
    await page.setUserAgent(
        'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36')
    await page.goto(url)  # 访问登录页面
    # 替换淘宝在检测浏览时采集的一些参数。
    # 就是在浏览器运行的时候，始终让window.navigator.webdriver=false
    # navigator是windiw对象的一个属性，同时修改plugins，languages，navigator 且让
    await page.evaluate(
        '''() =>{ Object.defineProperties(navigator,{ webdriver:{ get: () => false } }) }''')  # 以下为插入中间js，将淘宝会为了检测浏览器而调用的js修改其结果。
    await page.evaluate('''() =>{ window.navigator.chrome = { runtime: {},  }; }''')
    await page.evaluate('''() =>{ Object.defineProperty(navigator, 'languages', { get: () => ['en-US', 'en'] }); }''')
    await page.evaluate('''() =>{ Object.defineProperty(navigator, 'plugins', { get: () => [1, 2, 3, 4, 5,6], }); }''')

    # 使用type选定页面元素，并修改其数值，用于输入账号密码，修改的速度仿人类操作，因为有个输入速度的检测机制
    # 因为 pyppeteer 框架需要转换为js操作，而js和python的类型定义不同，所以写法与参数要用字典，类型导入
    await page.type(".fm-login-id", username, {'delay': input_time_random() - 50})
    await page.type("#J_StandardPwd input", pwd, {'delay': input_time_random()})

    # await page.screenshot({'path': './headless-test-result.png'})    # 截图测试
    time.sleep(2)

    # 检测页面是否有滑块。原理是检测页面元素。
    slider = await page.Jeval('#nocaptcha', 'node => node.style')  # 是否有滑块
    if slider:
        print('当前页面出现滑块')
        # await page.screenshot({'path': './headless-login-slide.png'}) # 截图测试
        flag, page = await mouse_slide(page=page)  # js拉动滑块过去。
        if flag:
            await page.keyboard.press('Enter')  # 确保内容输入完毕，少数页面会自动完成按钮点击
            print("print enter", flag)
            # await page.evaluate('''document.getElementsByClassName("btn").click()''')  # 如果无法通过回车键完成点击，就调用js模拟点击登录按钮。
            time.sleep(2)
            # cookies_list = await page.cookies()
            # print(cookies_list)
            await get_cookie(page)  # 导出cookie 完成登陆后就可以拿着cookie玩各种各样的事情了。
            # await page.close()  # 关闭页面防止报错OSError
    else:
        print("wwwwwww")
        await page.keyboard.press('Enter')
        print("print enter")
        # await page.evaluate('''document.getElementsByClassName("btn").click()''')
        await page.waitFor(20)
        await page.waitForNavigation()

        try:
            global error  # 检测是否是账号密码错误
            print("error_1:", error)
            error = await page.Jeval('.error', 'node => node.textContent')
            print("error_2:", error)
        except Exception as e:
            error = None
        finally:
            if error:
                print('确保账户安全重新入输入')
                # 程序退出。
                loop.close()
            else:
                print(page.url)
                await get_cookie(page)
    # time.sleep(100)


# 获取登录后cookie
async def get_cookie(page):
    # res = await page.content()
    cookies_list = await page.cookies()
    cookies = ''
    for cookie in cookies_list:
        str_cookie = '{0}={1};'
        str_cookie = str_cookie.format(cookie.get('name'), cookie.get('value'))
        cookies += str_cookie
    print(cookies)
    return cookies


def retry_if_result_none(result):
    return result is None


@retry(retry_on_result=retry_if_result_none, )
async def mouse_slide(page=None):
    await asyncio.sleep(2)
    try:
        # 鼠标移动到滑块，按下，滑动到头（然后延时处理），松开按键
        await page.hover('#nc_1_n1z')  # 不同场景的验证码模块能名字不同。
        await page.mouse.down()
        await page.mouse.move(2000, 0, {'delay': random.randint(1000, 2000)})
        await page.mouse.up()
    except Exception as e:
        print(e, ':验证失败')
        return None, page
    else:
        await asyncio.sleep(2)
        # 判断是否通过
        slider_again = await page.Jeval('.nc-lang-cnt', 'node => node.textContent')
        if slider_again != '验证通过':
            return None, page
        else:
            # await page.screenshot({'path': './headless-slide-result.png'}) # 截图测试
            print('验证通过')
            return 1, page


def input_time_random():
    return random.randint(100, 151)


if __name__ == '__main__':
    # username = '***'  # 淘宝用户名
    # pwd = '****'  # 密码
    username = 'qianyipei'
    pwd = 'qian120920hu'
    # 淘宝的迷你登录界面
    # url = 'https://login.taobao.com/member/login.jhtml?style=mini&css_style=b2b&from=b2b&full_redirect=true&redirect_url=https://login.1688.com/member/jump.htm?target=https://login.1688.com/member/marketSigninJump.htm?Done=http://login.1688.com/member/taobaoSellerLoginDispatch.htm&reg= http://member.1688.com/member/join/enterprise_join.htm?lead=http://login.1688.com/member/taobaoSellerLoginDispatch.htm&leadUrl=http://login.1688.com/member/'
    url = 'http://www.taobao.com'
    loop = asyncio.get_event_loop()  # 协程，开启个无限循环的程序流程，把一些函数注册到事件循环上。当满足事件发生的时候，调用相应的协程函数。
    loop.run_until_complete(main(username, pwd, url))  # 将协程注册到事件循环，并启动事件循环

import os
import time
import random
import asyncio
import pyppeteer


class LoginTaoBao:

    def __init__(self):
        os.environ['PYPPETEER_CHROMIUM_REVISION'] = '575458'
        pyppeteer.DEBUG = True
        self.page = None

    async def _injection_js(self):
        """
        注入js 突破淘宝封锁
        :return:
        """

        # 这个是关键参数, 主要靠这个
        await self.page.evaluate('''() =>{

                   Object.defineProperties(navigator,{
                     webdriver:{
                       get: () => false
                     }
                   })
                }''')

        await self.page.evaluate('''() => {
            window.navigator.chrome = {
            runtime: {},
            // etc.
            };
            }''')

        await self.page.evaluate('''() => {
                  const originalQuery = window.navigator.permissions.query;
                  return window.navigator.permissions.query = (parameters) => (
                    parameters.name === 'notifications' ?
                      Promise.resolve({ state: Notification.permission }) :
                      originalQuery(parameters)
                  );
                }
            ''')

        await self.page.evaluate('''() =>{
            Object.defineProperty(navigator, 'languages', {
            get: () => ['en-US', 'en']
                });
            }''')

        await self.page.evaluate('''() =>{
            Object.defineProperty(navigator, 'plugins', {
            get: () => [1, 2, 3, 4, 5],
                });
            }''')

    async def _init(self):
        """
        初始化浏览器
        :return:
        """
        # 设置浏览器参数
        browser = await pyppeteer.launch({'headless': False,
                                          'args': [
                                              '--disable-extensions',
                                              '--hide-scrollbars',
                                              '--disable-bundled-ppapi-flash',
                                              '--mute-audio',
                                              '--no-sandbox',
                                              '--disable-setuid-sandbox',
                                              '--disable-gpu',
                                          ],
                                          'dumpio': True,
                                          })
        # 创建浏览器对象
        self.page = await browser.newPage()
        # 设置浏览器头部
        await self.page.setUserAgent('Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 '
                                     '(KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36 Edge/16.16299')
        # 设置浏览器大小
        await self.page.setViewport({'width': 1080, 'height': 960})

    async def get_cookie(self):
        cookies_list = await self.page.cookies()
        cookies = ''
        for cookie in cookies_list:
            str_cookie = '{0}={1};'
            str_cookie = str_cookie.format(cookie.get('name'), cookie.get('value'))
            cookies += str_cookie
        print(cookies)
        return cookies

    async def mouse_slider(self):
        """
        滑动滑块
        :return: None 滑动失败 或者 True滑动成功
        """
        await asyncio.sleep(3)
        try:
            await self.page.hover('#nc_1_n1z')
            # 鼠标按下按钮
            await self.page.mouse.down()
            # 移动鼠标
            await self.page.mouse.move(1000, 0, {'steps': 30})
            # 松开鼠标
            await self.page.mouse.up()
            await asyncio.sleep(2)
        except Exception as e:
            print(e, '      :slider login error')
            return None
        else:
            await asyncio.sleep(3)
            # 获取元素内容
            ua = await self.page.evaluate('navigator.webdriver')
            print(ua)
            await self.page.screenshot({'path': './headless-slide-result.png'})
            slider_again = await self.page.querySelectorEval('#nc_1__scale_text', 'node => node.textContent')
            if slider_again != '验证通过':
                return None
            else:
                # 截图
                await self.page.screenshot({'path': './headless-slide-result.png'})
                print('验证通过')
                return True

    async def main(self, username_, pwd_):
        """
        登陆并获取cookie
        :param username_: 账号
        :param pwd_: 密码
        :return: cookie 或 None
        """
        # 初始化浏览器
        await self._init()
        # 打开淘宝登陆页面
        await self.page.goto('https://login.taobao.com')
        # 注入js
        await self._injection_js()
        # 点击密码登陆按钮
        await self.page.click('div.login-switch')
        time.sleep(random.random() * 2)
        # 输入用户名
        await self.page.type('#TPL_username_1', username_, {'delay': random.randint(100, 151) - 50})
        # 输入密码
        await self.page.type('#TPL_password_1', pwd_, {'delay': random.randint(100, 151)})
        time.sleep(random.random() * 2)
        # 获取滑块元素
        slider = await self.page.querySelector('#nc_1__scale_text')
        if slider:
            print('有滑块')
            # 移动滑块
            flag = await self.mouse_slider()
            # 判断滑块是否滑动成功
            if not flag:
                print('滑动滑块失败')
                return None
            time.sleep(random.random() + 0.5)
            # 点击登陆
            await self.page.click('#J_SubmitStatic')
        else:
            print('没滑块')
            # 按下回车
            await self.page.keyboard.press('Enter')
        # 等待
        await self.page.waitFor(20)
        # 等待导航, 等待跳转
        await self.page.waitForNavigation()
        # 判断是否登陆成功
        is_login = await self.page.querySelector('div .member')
        if is_login:
            print('登陆成功')
            cookie = await self.get_cookie()
            return cookie
        else:
            print('账号或密码错误, 登陆失败')
            return None


if __name__ == '__main__':
    # username = '韩梅梅'
    # pwd = '123456'
    username = 'qianyipei'
    pwd = 'qian120920hu'
    login = LoginTaoBao()
    loop = asyncio.get_event_loop()
    task = asyncio.ensure_future(login.main(username, pwd))
    loop.run_until_complete(task)
    print(task.result())
    """
    目前只能在有头模式下运行
    """





import asyncio
import csv
import random
import time
import json
import requests
import traceback
from utils.db_mongo import MongoOperation
from func_timeout import func_timeout
from func_timeout.exceptions import FunctionTimedOut
from pyppeteer import launch
from retrying import retry
from utils.logger import LOGGER
from utils.cookie_utils import retry_if_result_none, cookie_str_to_dict
from utils.settings import COL_ACCOUNTS


class Login():
    '''
    淘宝账号模拟登录类(通过登录生意参谋, 向易打单授权)
    '''
    def __init__(self):
        self.url = 'https://login.taobao.com/member/login.jhtml?style=mini&from=dongfeng&full_redirect=true&disableQuickLogin=true&redirectURL=http://unidesk.taobao.com&css_style=dongfeng&newMini2=true'
        self.mo = MongoOperation()
        self.check_interval = 3600
        self.accounts = []
        self.init_accounts()

    def init_accounts(self):
        '''
        初始化淘宝账号信息
        :return:
        '''
        with open('accounts.csv', 'r', encoding='utf-8') as f:
            filereader = csv.reader(f)
            for row in filereader:
                item ={}
                item['username'] = row[0]
                item['password'] = row[1]
                item['agent_name'] = row[2]
                item['status'] = int(row[3])
                print(item)
                self.mo.save_data(item, COL_ACCOUNTS)


    def login(self, account):
        '''
        登录
        :param account: 登录账号
        :return: 登录后的cookie字符串
        '''
        username = account.get('username')
        password = account.get('password')

        loop = asyncio.get_event_loop()
        task = asyncio.ensure_future(self.taobao_login(username, password))
        loop.run_until_complete(task)
        cookie = task.result()

        return cookie

    async def taobao_login(self, username, password):
        """
        淘宝登录主程序
        :param username: 用户名
        :param password: 密码
        :return: 登录cookies
        """
        # 'headless': False如果想要浏览器隐藏更改False为True
        browser = await launch({'headless': False, 'args': ['--no-sandbox']})
        page = await browser.newPage()
        await page.setUserAgent(
            'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36')
        await page.goto(self.url)

        # 以下为插入中间js，将淘宝会为了检测浏览器而调用的js修改其结果
        await page.evaluate('''() =>{ Object.defineProperties(navigator,{ webdriver:{ get: () => false } }) }''')
        await page.evaluate('''() =>{ window.navigator.chrome = { runtime: {},  }; }''')
        await page.evaluate(
            '''() =>{ Object.defineProperty(navigator, 'languages', { get: () => ['en-US', 'en'] }); }''')
        await page.evaluate(
            '''() =>{ Object.defineProperty(navigator, 'plugins', { get: () => [1, 2, 3, 4, 5,6], }); }''')

        time.sleep(1)

        # 输入用户名，密码
        await page.click('#fm-login-id')
        await page.type('#fm-login-id', username, {'delay': self.input_time_random() - 50})  # delay是限制输入的时间
        await page.type('#fm-login-password', password, {'delay': self.input_time_random()})

        # 检测页面是否有滑块。原理是检测页面元素。
        slider = await page.xpath('//div[@id="nocaptcha"]')  # 是否有滑块
        if slider:
            print('当前页面出现滑块')
            # await page.screenshot({'path': './headless-login-slide.png'}) # 截图测试
            flag, page = await self.mouse_slide(page=page)  # js拉动滑块过去。

        # await page.click('button.fm-button')
        await page.click('button.fm-button')
        await page.waitForNavigation()

        time.sleep(2)

        if len(page.frames) > 1:
            frame = page.frames[1]

            phone_code_btn = await frame.xpath('//input[contains(@class, "send-code-btn")]')
            if phone_code_btn:
                print('当前页面出现短信验证, 需人工处理')
                try:
                    func_timeout(300, self.get_phone_code_by_hand)
                except FunctionTimedOut:
                    await self.close_client(page, browser)
                    cookie = ''
                    return cookie


        time.sleep(2)
        cookie = await self.get_cookie(page)

        await self.close_client(page, browser)
        print(cookie)
        return cookie

    def get_phone_code_by_hand(self):
        '''
        手动处理验证码
        :return:
        '''
        input('登录成功后按回车.')

    async def close_client(self, page, browser):
        '''
        关闭浏览器
        :param page:
        :param browser:
        :return:
        '''
        try:
            await page.close()
            await browser.close()
        except:
            pass

    async def get_cookie(self, page):
        '''
        获取登录后cookie(字符串形式)
        :param page: 页面对象
        :return:
        '''
        cookies_list = await page.cookies()
        cookies = ''
        for cookie in cookies_list:
            str_cookie = '{0}={1};'
            str_cookie = str_cookie.format(cookie.get('name'), cookie.get('value'))
            cookies += str_cookie

        return cookies

    @retry(retry_on_result=retry_if_result_none)
    async def mouse_slide(self, page=None):
        '''
        鼠标滑动
        :param page: 页面对象
        :return:
        '''
        await asyncio.sleep(2)
        try:
            # 鼠标移动到滑块，按下，滑动到头（然后延时处理），松开按键
            await page.hover('#nc_2_n1z')  # 不同场景的验证码模块能名字不同。
            await page.mouse.down()
            await page.mouse.move(2000, 0, {'delay': random.randint(1000, 2000)})
            await page.mouse.up()
        except Exception as e:
            print(e, ':验证失败')
            return None, page
        else:
            await asyncio.sleep(2)
            # 判断是否通过
            slider_again = await page.Jeval('.nc-lang-cnt', 'node => node.textContent')
            if slider_again != '验证通过':
                return None, page
            else:
                # await page.screenshot({'path': './headless-slide-result.png'}) # 截图测试
                print('验证通过')
                return 1, page

    def get_brand_list(self,cookie):
        url = 'https://unidesk.taobao.com/api/bp/brand/list?'
        cookies_dict = cookie_str_to_dict(cookie)
        params = {
            't': int(time.time() * 1000),
            'bizCode': 'uniDeskGlobal',
            'pageNo': 1,
            'pageSize': 20,
            'keyWord': '',
            # 'cdnVersion':''
        }

        headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36',
            'Cookie': cookie,
            'X-XSRF-TOKEN': cookies_dict['XSRF-TOKEN'],
        }
        resp = requests.get(url=url, params=params, headers=headers, verify=False)
        result = json.loads(resp.text)
        if result.get('data') and result.get('data').get('list'):
            data_list = result.get('data').get('list')
            brand_info = []
            for data in data_list:
                item = {}
                brandName = data.get('name')
                id = data.get('id')
                item['brandName'] = brandName
                item['id'] = id
                brand_info.append(item)
            return brand_info

    def input_time_random(self):
        return random.randint(100, 151)

    def check_if_expire(self, cookies):
        '''
        检测是否过期
        :return:
        '''
        cookies_dict = cookie_str_to_dict(cookies)
        if cookies == '':
            return True
        try:
            url = 'https://unidesk.taobao.com/api/bp/user/getLoginUser'
            headers = {
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36',
                'Cookie': cookies,
                'X-XSRF-TOKEN': cookies_dict['XSRF-TOKEN'],
            }
            resp = requests.get(url, headers=headers, verify=False)
            data = json.loads(resp.text)
            if data.get('data') and data.get('data').get('current'):
                return False
            else:
                return True

        except:
            return True

    def get_expire_accounts(self):
        '''
        获取过期账号
        :return:
        '''
        expire_accounts = []
        accounts = self.mo.get_data_list(COL_ACCOUNTS, {'status': 1})
        for account in accounts:
            cookies = account.get('cookie', '')
            if self.check_if_expire(cookies):
                expire_accounts.append(account)

        return expire_accounts

    def save(self, account, cookies,brand_list):

        '''
        存储cookie和品牌信息
        :param account: 账号
        :param cookies: cookie
        :return:
        '''
        account['cookie'] = cookies
        account['brand_list'] = brand_list
        self.mo.save_data(account, COL_ACCOUNTS)

    def run(self):
        expire_accounts = self.get_expire_accounts()
        for account in expire_accounts:
            LOGGER.info(f'账号 {account.get("username")} 过期, 正在重新登录...', if_print=True)
            try:
                cookies = self.login(account)
                brand_list = self.get_brand_list(cookies)
                self.save(account, cookies,brand_list)

                LOGGER.info(f'账号 {account.get("username")} 更新完成.', if_print=True)
            except:
                LOGGER.error(f'账号 {account.get("username")} 更新出错.')
                LOGGER.error(traceback.format_exc())

    def start(self):
        print('开始运行UniDesk模拟登录程序...')
        print(f'检测周期为 {self.check_interval} 秒')
        while 1:
            print(time.strftime("%Y-%m-%d %H:%M:%S").center(20, '-'))
            self.init_accounts()
            self.run()
            time.sleep(self.check_interval)


if __name__ == '__main__':
    login = Login()
    login.start()







