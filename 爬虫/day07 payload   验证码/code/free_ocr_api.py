#!/usr/bin/env python 
# -*- coding:utf-8 -*-
#  每天只能请求500次
import json
import traceback

import requests


def ocr_space_file(filename, overlay=False, api_key='helloworld', language='eng'):
    """ OCR.space API request with local file.
        Python3.5 - not tested on 2.7
    :param filename: Your file path & name.
    :param overlay: Is OCR.space overlay required in your response.
                    Defaults to False.
    :param api_key: OCR.space API key.
                    Defaults to 'helloworld'.
    :param language: Language code to be used in OCR.
                    List of available language codes can be found on https://ocr.space/OCRAPI
                    Defaults to 'en'.
    :return: Result in JSON format.
    """

    payload = {'isOverlayRequired': overlay,
               'apikey': api_key,
               'language': language,
               }
    with open(filename, 'rb') as f:
        r = requests.post('https://api.ocr.space/parse/image',
                          files={filename: f},
                          data=payload,
                          )

    result = json.loads(r.content.decode())
    try:
        content = result.get('ParsedResults')[0].get('ParsedText')
    except:
        content = traceback.format_exc()
    return content


def ocr_space_url(url, overlay=False, api_key='helloworld', language='eng'):
    """ OCR.space API request with remote file.
        Python3.5 - not tested on 2.7
    :param url: Image url.
    :param overlay: Is OCR.space overlay required in your response.
                    Defaults to False.
    :param api_key: OCR.space API key.
                    Defaults to 'helloworld'.
    :param language: Language code to be used in OCR.
                    List of available language codes can be found on https://ocr.space/OCRAPI
                    Defaults to 'en'.
    :return: Result in JSON format.
    """

    payload = {'url': url,
               'isOverlayRequired': overlay,
               'apikey': api_key,
               'language': language,
               }
    r = requests.post('https://api.ocr.space/parse/image',
                      data=payload,
                      )



    return r.content.decode()


# Use examples:
# test_file = ocr_space_file(filename='example_image.png', language='pol')
test_file = ocr_space_file(filename='./images/996398.png', language='chs')
print(test_file)
# if test_file.get('ParsedResults') and test_file.get('ParsedResults')[0].get('ParsedText'):
#     text = test_file.get('ParsedResults')[0].get('ParsedText')
#     print(text)
# test_url = ocr_space_url(url='http://i.imgur.com/31d5L5y.jpg')
# print(test_url)