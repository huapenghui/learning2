#!/usr/bin/env python 
# -*- coding:utf-8 -*-
from celery import Celery

app = Celery('tasks',broker='redis://127.0.0.1:6379/0',backend='redis://127.0.0.1:6379/0')

# 'tasks'为module名称，broker为消息中间件，backend为任务结果存储地方（backend可以不设置，默认为disabledbackend）
#127.0.0.1:6379/0 为redis server安装的ip地址和端口，0为数据库编号（redis有0-16个数据库）

@app.task
def add(x, y):
    return x+y